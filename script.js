let createNewUser = () => {
  let firstName = prompt("Enter your first name");
  let lastName = prompt("Enter your last name");
  let birthDate = prompt(
    "Please enter you date of birth in the format dd.mm.yyyy.",
    "22.12.1992"
  );
  let currentDate = new Date();
  let birthDateSplitted = birthDate.split(".");
  let birthDay = new Date(
    `${birthDateSplitted[1]}.${birthDateSplitted[0]}.${birthDateSplitted[2]}`
  );

  return {
    firstName,
    lastName,
    birthDate: birthDay,
    getLogin: () => firstName[0].toLowerCase() + lastName.toLowerCase(),
    getPassword: () =>
      firstName[0].toUpperCase() + lastName.toLowerCase() + birthDate.slice(6),
    getAge: () => currentDate.getFullYear() - birthDay.getFullYear(),
  };
};

// console.log(createNewUser().getPassword())
// console.log(createNewUser().getLogin())
console.log(createNewUser().getAge());
